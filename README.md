# IPTool

IPTool is a Python package and CLI utility for manipulating and providing
information about IP addresses, networks, and masks. It is meant to be a
lightweight and helpful tool for network engineers, administrators, or anyone
who deals with IP network specification.

## Installing

Install and update using `pip`:

    $ pip install --update iptool

## Usage

Upon installation, an executable named `iptool` should exist in the user's
path, so that you can execute the script directly using:

    $ iptool [COMMAND]

However, if necessary, the module can be executed explicitly through the
Python interpreter as well using:

    $ python3 -m iptool [COMMAND]

The IPTool utility is broken down into several "commands" which are specified
as the first argument to the program (represented by `[COMMAND]` in the
examples above). Complete help can be obtained with the `--help` argument.

The commands available are:

- `cidr`: Display a table of IPv4 mask formats.

  If a mask is provided, the table will be truncated to include that mask
  and a few neighboring masks. If no mask is provided, the entire 33-row
  table is displayed.

  Examples:

        $ iptool cidr
        CIDR   Subnet Mask       Wildcard Mask
          /0   0.0.0.0           255.255.255.255
          /1   128.0.0.0         127.255.255.255
          /2   192.0.0.0         63.255.255.255
          /3   224.0.0.0         31.255.255.255
          /4   240.0.0.0         15.255.255.255
          /5   248.0.0.0         7.255.255.255
        [output truncated]

        $ iptool cidr /24
        CIDR   Subnet Mask       Wildcard Mask
         /21   255.255.248.0     0.0.7.255
         /22   255.255.252.0     0.0.3.255
         /23   255.255.254.0     0.0.1.255
         /24   255.255.255.0     0.0.0.255
         /25   255.255.255.128   0.0.0.127
         /26   255.255.255.192   0.0.0.63
         /27   255.255.255.224   0.0.0.31

        $ iptool cidr 254.0

- `merge`: Merge multiple networks together, summarizing where possible

  If no networks are specified, they will be read from STDIN, allowing the
  tool to be used with pipes or chaining.

  Examples:

        $ iptool merge 10.0.0.0/24 10.0.1.0/24 10.0.2.0/24 10.0.4.0/24 10.0.5.0/24
        10.0.0.0/23
        10.0.2.0/24
        10.0.4.0/23

        $ iptool merge 10.0.0.0/24 10.0.1.0/24 DEAD:BEEF::/64 DEAD:BEEF:0:1::/64
        10.0.0.0/23
        dead:beef::/63

- `supernet`: Supernet all networks into one network per version. Any number of
  IPv4 networks will result in a single IPv4 network output, and any number
  of IPv6 networks will result in a single IPv6 network output. However, if
  you provide both an IPv4 network and an IPv6 network, both an IPv4 and an
  IPv6 network will be output.

  If no networks are specified, they will be read from STDIN, allowing the
  tool to be used with pipes or chaining.

  Examples:

        $ iptool supernet 10.0.0.0/24 10.0.1.0/24 10.0.2.0/24 10.0.4.0/24 10.0.5.0/24
        10.0.0.0/21

        $ iptool supernet 10.0.0.0/24 10.0.1.0/24 DEAD:BEEF::/64 DEAD:BEEF:0:1::/64
        dead:beef::/63
