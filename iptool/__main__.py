"""CLI Interface for `iptool` package"""

# Copyright (C) 2021, Matthew Wyant

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from ipaddress import collapse_addresses
import sys
from typing import Optional, Iterable, List

import click
from rich.console import Console
from rich.text import Text

import iptool


def read_lines_from_stdin() -> Iterable[str]:
    """Read from stdin, splitting on newlines, until a blank line is found"""
    for line in sys.stdin:
        line = line.strip()

        if not line:
            return

        yield line


def read_tokens_from_stdin() -> Iterable[str]:
    """Read from stdin, splitting on whitespace, until a blank line is found"""
    for line in read_lines_from_stdin():
        for token in line.split():
            yield token.strip()


@click.group(context_settings=iptool.CLICK_CONTEXT)
def cli():
    pass


VERSION_STYLES = {
    4: "bright_blue",
    6: "bright_magenta",
}


@cli.command(context_settings=iptool.CLICK_CONTEXT)
@click.argument("network", nargs=-1)
def merge(network: Optional[List[str]] = None):
    """Merge multiple networks together, summarizing where possible

    If no networks are specified, they will be read from STDIN"""

    if not network:
        network = list(read_tokens_from_stdin())

    if not network:
        return

    console = Console()

    all_nets = iptool.IPBin(iptool.normalize_to_networks(network))
    for v, nets in all_nets.by_version:
        for merged in sorted(collapse_addresses(nets)):
            console.print(Text(str(merged)), style=VERSION_STYLES[v])


@cli.command(context_settings=iptool.CLICK_CONTEXT)
@click.argument("network", nargs=-1)
def supernet(network: Optional[List[str]] = None):
    """Supernet all networks into one network per version

    If no networks are specified, they will be read from STDIN"""

    if not network:
        network = list(read_tokens_from_stdin())

    if not network:
        return

    console = Console()

    all_nets = iptool.normalize_to_networks(network)
    all_nets = iptool.IPBin(all_nets)

    for v, nets in all_nets.by_version:
        if nets:
            supernet = iptool.supernet(nets)
            console.print(Text(str(supernet)), style=VERSION_STYLES[v])


@cli.command(context_settings=iptool.CLICK_CONTEXT)
@click.argument("mask", required=False)
def cidr(mask: Optional[str] = None):
    """Display a table of IPv4 mask formats.

    If a mask is provided, the table will be truncated to include that mask
    and a few neighboring masks. If no mask is provided, the entire 33-row
    table is displayed.

    The provided mask can be in CIDR, subnet, or wildcard format, or an
    abbreviation of one of them. The provided mask is resolved according to
    `iptool.identify_mask()`"""

    cidr: Optional[int] = None
    cidr_start: int = 0
    cidr_end: int = 32

    table_offset = 3

    if mask:
        cidr = iptool.identify_mask(mask)

        cidr_end = cidr + table_offset
        cidr_start = cidr - table_offset

        if cidr_end > 32:
            cidr_end = 32
            cidr_start = cidr_end - 2 * table_offset

        if cidr_start < 0:
            cidr_start = 0
            cidr_end = cidr_start + 2 * table_offset

    console = Console()

    titles = ["CIDR", "Subnet Mask", "Wildcard Mask"]
    min_widths = [4, 15, 15]

    widths = [
        str(max(len(title), min_width))
        for title, min_width in zip(titles, min_widths)
    ]

    widths[0] = f">{widths[0]}"
    widths[-1] = ""

    row = Text()

    for x, (title, width) in enumerate(zip(titles, widths)):
        if x > 0:
            row.append("   ")
        row.append(f"{title:{width}s}", style="bold white")
    console.print(row)

    for c in range(cidr_start, cidr_end + 1):
        m = iptool.IPv4_MASKS[c]

        fields = (
            f"/{c:d}",
            m.subnet,
            m.wildcard,
        )
        styles = (
            "magenta",
            "blue",
            "green",
        )

        row = Text()

        for x, (field, width, style) in enumerate(zip(fields, widths, styles)):
            if x > 0:
                row.append("   ")

            if cidr == c:
                row.append(f"{field:{width}s}", style=f"bold bright_{style}")
            else:
                row.append(f"{field:{width}s}", style=style)

        console.print(row)


if __name__ == "__main__":
    cli()
