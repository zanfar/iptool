"""CLI Tool for Manipulating IP Addresses and Masks"""

# Copyright (C) 2021, Matthew Wyant

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__author__ = "Matthew Wyant"
__email__ = "me@matthewwyant.com"
__copyright__ = "Copyright 2021, Matthew Wyant"
__credits__ = [__author__]
__license__ = "GPLv3"
__version__ = "0.1.0"
__version_info__ = (0, 1, 0)
__maintainer__ = __author__
__status__ = "Prototype"

from dataclasses import dataclass
from ipaddress import ip_interface, get_mixed_type_key, collapse_addresses
from ipaddress import IPv4Address, IPv4Network, IPv4Interface
from ipaddress import IPv6Address, IPv6Network, IPv6Interface
import ipaddress
import re
from typing import overload, NamedTuple, Union, Iterable, Iterator
from typing import Dict, List, Tuple, Any, Optional

CLICK_CONTEXT = {"help_option_names": ["-h", "--help"]}

ip_address_T = Union[IPv4Address, IPv6Address]
ip_network_T = Union[IPv4Network, IPv6Network]
ip_interface_T = Union[IPv4Interface, IPv6Interface]

ipv4_T = Union[IPv4Address, IPv4Network, IPv4Interface]
ipv6_T = Union[IPv6Address, IPv6Network, IPv6Interface]
ip_T = Union[ipv4_T, ipv6_T]


def normalize_to_networks(identifiers: Iterable[Any]) -> List[ip_network_T]:
    """Convert a sequence of ip-network-like items to formal ip network
        identifiers of known types

    Accepts any input that `ipaddress.ip_interface` will accept, which is
        basically strings and other `ipaddress` types. If no network mask
        is provided, a /32 or /128 will be assumed. Regardless of the ip
        part provided, only the network address will be returned."""
    return [ip_interface(id).network for id in identifiers]


def normalize_to_address(identifiers: Iterable[Any]) -> List[ip_address_T]:
    """Convert a sequence of ip-address-like items to formal ip address
        identifiers of known types

    Accepts any input that `ipaddress.ip_interface` will accept, which is
        basically strings and other `ipaddress` types. All mask information
        will be stripped."""
    return [ip_interface(id).ip for id in identifiers]


def normalize_to_interface(identifiers: Iterable[Any]) -> List[ip_interface_T]:
    """Convert a sequence of ip-address-like items to formal ip interface
        identifiers of known types

    Accepts any input that `ipaddress.ip_interface` will accept, which is
        basically strings and other `ipaddress` types. If no network mask
        is provided, a /32 or /128 will be assumed."""
    return [ip_interface(id) for id in identifiers]


class IPBin:
    """Storage for mixed `ipaddress` types that allows access by protocol
    version, purpose, or both"""

    def __init__(self, group: Optional[Iterable[ip_T]] = None):
        self._items = []
        if group:
            self._items = list(group)
            self._items.sort(key=get_mixed_type_key)  # type: ignore[reportGeneralTypeIssues]

    def _by_type(self, *types: type) -> Iterator:
        for item in self._items:
            if type(item) in types:
                yield item

    @property
    def v4(self) -> Iterator[ipv4_T]:
        yield from self._by_type(IPv4Address, IPv4Network, IPv4Interface)

    @property
    def v6(self) -> Iterator[ipv6_T]:
        yield from self._by_type(IPv6Address, IPv6Network, IPv6Interface)

    @property
    def addresses(self) -> Iterator[ip_address_T]:
        yield from self._by_type(IPv4Address, IPv6Address)

    @property
    def interfaces(self) -> Iterator[ip_interface_T]:
        yield from self._by_type(IPv4Interface, IPv6Interface)

    @property
    def networks(self) -> Iterator[ip_network_T]:
        yield from self._by_type(IPv4Network, IPv6Network)

    @property
    def v4addresses(self) -> Iterator[IPv4Address]:
        yield from self._by_type(IPv4Address)

    @property
    def v4interfaces(self) -> Iterator[IPv4Network]:
        yield from self._by_type(IPv4Network)

    @property
    def v4networks(self) -> Iterator[IPv4Interface]:
        yield from self._by_type(IPv4Interface)

    @property
    def v6addresses(self) -> Iterator[IPv6Address]:
        yield from self._by_type(IPv6Address)

    @property
    def v6interfaces(self) -> Iterator[IPv6Network]:
        yield from self._by_type(IPv6Network)

    @property
    def v6networks(self) -> Iterator[IPv6Interface]:
        yield from self._by_type(IPv6Interface)

    @property
    def by_version(self) -> Iterator[Tuple[int, List]]:
        yield 4, list(self.v4)
        yield 6, list(self.v6)


@overload
def supernet(networks: Iterable[IPv4Network]) -> IPv4Network:
    ...


@overload
def supernet(networks: Iterable[IPv6Network]) -> IPv6Network:
    ...


def supernet(networks):
    """Find a single supernet for the provided networks

    Exactly one network will be returned that will include all provided
    network space, but might also include extra address space.
    Compare to `merge` which might return multiple networks, but covers
    no additional space."""

    i = iter(networks)
    supernet = next(i)

    for net in i:
        while not supernet.supernet_of(net):
            supernet = supernet.supernet()

    return supernet


@overload
def merge(networks: Iterable[IPv4Network]) -> Iterator[IPv4Network]:
    ...


@overload
def merge(networks: Iterable[IPv6Network]) -> Iterator[IPv6Network]:
    ...


def merge(networks):
    """Find the fewest networks that include the provided networks

    At least one network will be returned that will include all provided
    network space and no extra, but multiple networks may be needed to meet
    that criteria.
    Compare to `supernet` which might include extra space, but only returns
    a single network."""

    return collapse_addresses(networks)


RE_CIDR = re.compile(r"^\/?(?P<length>3[0-2]|[0-2]?[0-9])$")


@dataclass
class MaskInfo:
    subnet: str
    wildcard: str


IPv4_MASKS: Dict[int, MaskInfo] = {
    0: MaskInfo("0.0.0.0", "255.255.255.255"),
    1: MaskInfo("128.0.0.0", "127.255.255.255"),
    2: MaskInfo("192.0.0.0", "63.255.255.255"),
    3: MaskInfo("224.0.0.0", "31.255.255.255"),
    4: MaskInfo("240.0.0.0", "15.255.255.255"),
    5: MaskInfo("248.0.0.0", "7.255.255.255"),
    6: MaskInfo("252.0.0.0", "3.255.255.255"),
    7: MaskInfo("254.0.0.0", "1.255.255.255"),
    8: MaskInfo("255.0.0.0", "0.255.255.255"),
    9: MaskInfo("255.128.0.0", "0.127.255.255"),
    10: MaskInfo("255.192.0.0", "0.63.255.255"),
    11: MaskInfo("255.224.0.0", "0.31.255.255"),
    12: MaskInfo("255.240.0.0", "0.15.255.255"),
    13: MaskInfo("255.248.0.0", "0.7.255.255"),
    14: MaskInfo("255.252.0.0", "0.3.255.255"),
    15: MaskInfo("255.254.0.0", "0.1.255.255"),
    16: MaskInfo("255.255.0.0", "0.0.255.255"),
    17: MaskInfo("255.255.128.0", "0.0.127.255"),
    18: MaskInfo("255.255.192.0", "0.0.63.255"),
    19: MaskInfo("255.255.224.0", "0.0.31.255"),
    20: MaskInfo("255.255.240.0", "0.0.15.255"),
    21: MaskInfo("255.255.248.0", "0.0.7.255"),
    22: MaskInfo("255.255.252.0", "0.0.3.255"),
    23: MaskInfo("255.255.254.0", "0.0.1.255"),
    24: MaskInfo("255.255.255.0", "0.0.0.255"),
    25: MaskInfo("255.255.255.128", "0.0.0.127"),
    26: MaskInfo("255.255.255.192", "0.0.0.63"),
    27: MaskInfo("255.255.255.224", "0.0.0.31"),
    28: MaskInfo("255.255.255.240", "0.0.0.15"),
    29: MaskInfo("255.255.255.248", "0.0.0.7"),
    30: MaskInfo("255.255.255.252", "0.0.0.3"),
    31: MaskInfo("255.255.255.254", "0.0.0.1"),
    32: MaskInfo("255.255.255.255", "0.0.0.0"),
}


def identify_mask(ident: str) -> int:
    """Attempt to identify the IPv4 mask length a particular string
    representation refers to

    CIDR, subnet mask, and wildcard mask representations are identified as
    expected, but partial representations are identified as best as possible
    as well. In the case of ambiguity, a subnet mask is assumed over a
    wildcard mask, and a longer mask is assumed over a shorter mask.

    Raises ValueError if the identifier cannot be resolved"""

    if m := RE_CIDR.match(ident):
        return int(m.group("length"))

    parts = [p for p in ident.split(".") if p != ""]

    expanded_subnet = ".".join(["255"] * (4 - len(parts)) + parts)
    expanded_wildcard = ".".join(["0"] * (4 - len(parts)) + parts)
    possible_wildcard = None

    for cidr, mask in IPv4_MASKS.items():
        if expanded_subnet == mask.subnet:
            return cidr
        if expanded_wildcard == mask.wildcard:
            possible_wildcard = cidr

    if possible_wildcard:
        return possible_wildcard

    raise ValueError(f"Unknown mask format '{ident}'")
